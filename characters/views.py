from django.views.generic import DetailView, ListView, CreateView, UpdateView, DeleteView
from characters.models import CharacterSheet
import math

# Create your views here.

class CharacterDetailView(DetailView):
    model = CharacterSheet
    template_name = "characters/detail.html"

class CharacterListView(ListView):
    model = CharacterSheet
    template_name = "characters/list.html"

class CharacterCreateView(CreateView):
    model = CharacterSheet

class CharacterUpdateView(UpdateView):
    model = CharacterSheet

class CharacterDeleteView(DeleteView):
    model = CharacterSheet

# class AbilityDetailView(DetailView):
#     model = AbilityModifiers
#     template_name = "characters/detail.html"