from django.contrib import admin
from characters.models import CharacterSheet

# Register your models here.

class CharacterSheetAdmin(admin.ModelAdmin):
    pass

admin.site.register(CharacterSheet, CharacterSheetAdmin)
