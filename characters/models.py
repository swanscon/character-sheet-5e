from django.db import models
from django.conf import settings
import math

USER_MODEL = settings.AUTH_USER_MODEL

# Create your models here.

class CharacterSheet(models.Model):
    name = models.CharField(max_length=40)
    character_class = models.CharField(max_length=20)
    race = models.CharField(max_length=30)
    background = models.CharField(max_length=30)
    experience = models.CharField(max_length=30)
    level = models.PositiveSmallIntegerField(default=1)
    strength = models.PositiveSmallIntegerField(default=10)
    dexterity = models.PositiveSmallIntegerField(default=10)
    constitution = models.PositiveSmallIntegerField(default=10)
    intelligence = models.PositiveSmallIntegerField(default=10)
    wisdom = models.PositiveSmallIntegerField(default=10)
    charisma = models.PositiveSmallIntegerField(default=10)
    armor_class = models.PositiveSmallIntegerField(default=10)
    hit_points = models.PositiveSmallIntegerField(default=10)
    speed = models.PositiveSmallIntegerField(default=30)
    player = models.ForeignKey(
        USER_MODEL,
        related_name = "characters",
        on_delete=models.CASCADE,
        null=True,
    )
    str_save_proficiency = models.BooleanField(default=False)
    dex_save_proficiency = models.BooleanField(default=False)
    con_save_proficiency = models.BooleanField(default=False)
    int_save_proficiency = models.BooleanField(default=False)
    wis_save_proficiency = models.BooleanField(default=False)
    cha_save_proficiency = models.BooleanField(default=False)

    acrobatics = models.BooleanField(default=False)
    animal_handling = models.BooleanField(default=False)
    arcana = models.BooleanField(default=False)
    athletics = models.BooleanField(default=False)
    deception = models.BooleanField(default=False)
    history = models.BooleanField(default=False)
    insight = models.BooleanField(default=False)
    intimidation = models.BooleanField(default=False)
    investigation = models.BooleanField(default=False)
    medicine = models.BooleanField(default=False)
    nature = models.BooleanField(default=False)
    perception = models.BooleanField(default=False)
    performance = models.BooleanField(default=False)
    persuasion = models.BooleanField(default=False)
    religion = models.BooleanField(default=False)
    sleight_of_hand = models.BooleanField(default=False)
    stealth = models.BooleanField(default=False)
    survival = models.BooleanField(default=False)

    acrobatics_ex = models.BooleanField(default=False)
    animal_handling_ex = models.BooleanField(default=False)
    arcana_ex = models.BooleanField(default=False)
    athletics_ex = models.BooleanField(default=False)
    deception_ex = models.BooleanField(default=False)
    history_ex = models.BooleanField(default=False)
    insight_ex = models.BooleanField(default=False)
    intimidation_ex = models.BooleanField(default=False)
    investigation_ex = models.BooleanField(default=False)
    medicine_ex = models.BooleanField(default=False)
    nature_ex = models.BooleanField(default=False)
    perception_ex = models.BooleanField(default=False)
    performance_ex = models.BooleanField(default=False)
    persuasion_ex = models.BooleanField(default=False)
    religion_ex = models.BooleanField(default=False)
    sleight_of_hand_ex = models.BooleanField(default=False)
    stealth_ex = models.BooleanField(default=False)
    survival_ex = models.BooleanField(default=False)


    def __str__(self):
        return self.name + " by " + str(self.player)

    def proficiency(self):
        result = 0
        if self.level <= 4:
            result = 2
        elif self.level <= 8:
            result = 3
        elif self.level <= 12:
            result = 4
        elif self.level <= 16:
            result = 5
        elif self.level <= 20:
            result = 6
        return result
    
    def str_mod(self):
        result = 0
        if self.strength < 10:
            result = math.trunc(((self.strength - 10) / 2) - 0.5)
        else:
            result = math.trunc((self.strength - 10) / 2)
        return result
    
    def dex_mod(self):
        result = 0
        if self.dexterity < 10:
            result = math.trunc(((self.dexterity - 10) / 2) - 0.5)
        else:
            result = math.trunc((self.dexterity - 10) / 2)
        return result
    
    def con_mod(self):
        result = 0
        if self.constitution < 10:
            result = math.trunc(((self.constitution - 10) / 2) - 0.5)
        else:
            result = math.trunc((self.constitution - 10) / 2)
        return result
    
    def int_mod(self):
        result = 0
        if self.intelligence < 10:
            result = math.trunc(((self.intelligence - 10) / 2) - 0.5)
        else:
            result = math.trunc((self.intelligence - 10) / 2)
        return result
    
    def wis_mod(self):
        result = 0
        if self.wisdom < 10:
            result = math.trunc(((self.wisdom - 10) / 2) - 0.5)
        else:
            result = math.trunc((self.wisdom - 10) / 2)
        return result
    
    def cha_mod(self):
        result = 0
        if self.charisma < 10:
            result = math.trunc(((self.charisma - 10) / 2) - 0.5)
        else:
            result = math.trunc((self.charisma - 10) / 2)
        return result
# Fix saves, return str with (-) or (+)
    def str_save(self):
        result = 0
        if self.str_save_proficiency == True:
            result = self.str_mod + self.proficiency
        else:
            result = self.str_mod
        return result

    def dex_save(self):
        result = 0
        if self.dex_save_proficiency == True:
            result = self.dex_mod + self.proficiency
        else:
            result = self.dex_mod
        return result

    def con_save(self):
        result = 0
        if self.con_save_proficiency == True:
            result = self.con_mod + self.proficiency
        else:
            result = self.con_mod
        return result
    
    def int_save(self):
        result = 0
        if self.int_save_proficiency == True:
            result = self.int_mod + self.proficiency
        else:
            result = self.int_mod
        return result
    
    def wis_save(self):
        result = 0
        if self.wis_save_proficiency == True:
            result = self.wis_mod + self.proficiency
        else:
            result = self.wis_mod
        return result
    
    def cha_save(self):
        result = 0
        if self.cha_save_proficiency == True:
            result = self.cha_mod + self.proficiency
        else:
            result = self.cha_mod
        return result
# Fix skills, return str with (-) or (+)
    def acrobatics_skill(self):
        result = 0
        if self.acrobatics == True:
            result = self.dex_mod + self.proficiency
        elif self.acrobatics_ex == True:
            result = self.dex_mod + ((self.proficiency)*2)
        else:
            result = self.dex_mod
        return result
    
    def animal_handling_skill(self):
        result = 0
        if self.animal_handling == True:
            result = self.wis_mod + self.proficiency
        elif self.animal_handling_ex == True:
            result = self.wis_mod + ((self.proficiency)*2)
        else:
            result = self.wis_mod
        return result
    
    def arcana_skill(self):
        result = 0
        if self.arcana == True:
            result = self.int_mod + self.proficiency
        elif self.arcana_ex == True:
            result = self.int_mod + ((self.proficiency)*2)
        else:
            result = self.int_mod
        return result
    
    def athletics_skill(self):
        result = 0
        if self.athletics == True:
            result = self.str_mod + self.proficiency
        elif self.athletics_ex == True:
            result = self.str_mod + ((self.proficiency)*2)
        else:
            result = self.str_mod
        return result
    
    def deception_skill(self):
        result = 0
        if self.deception == True:
            result = self.cha_mod + self.proficiency
        elif self.deception_ex == True:
            result = self.cha_mod + ((self.proficiency)*2)
        else:
            result = self.cha_mod
        return result
    
    def history_skill(self):
        result = 0
        if self.history == True:
            result = self.int_mod + self.proficiency
        elif self.history_ex == True:
            result = self.int_mod + ((self.proficiency)*2)
        else:
            result = self.int_mod
        return result
    
    def insight_skill(self):
        result = 0
        if self.insight == True:
            result = self.wis_mod + self.proficiency
        elif self.insight_ex == True:
            result = self.wis_mod + ((self.proficiency)*2)
        else:
            result = self.wis_mod
        return result
    
    def intimidation_skill(self):
        result = 0
        if self.intimidation == True:
            result = self.cha_mod + self.proficiency
        elif self.intimidation_ex == True:
            result = self.cha_mod + ((self.proficiency)*2)
        else:
            result = self.cha_mod
        return result
    
    def investigation_skill(self):
        result = 0
        if self.investigation == True:
            result = self.int_mod + self.proficiency
        elif self.investigation_ex == True:
            result = self.int_mod + ((self.proficiency)*2)
        else:
            result = self.int_mod
        return result
    
    def medicine_skill(self):
        result = 0
        if self.medicine == True:
            result = self.wis_mod + self.proficiency
        elif self.medicine_ex == True:
            result = self.wis_mod + ((self.proficiency)*2)
        else:
            result = self.wis_mod
        return result
    
    def nature_skill(self):
        result = 0
        if self.nature == True:
            result = self.int_mod + self.proficiency
        elif self.nature_ex == True:
            result = self.int_mod + ((self.proficiency)*2)
        else:
            result = self.int_mod
        return result
    
    def perception_skill(self):
        result = 0
        if self.perception == True:
            result = self.wis_mod + self.proficiency
        elif self.perception_ex == True:
            result = self.wis_mod + ((self.proficiency)*2)
        else:
            result = self.wis_mod
        return result
    
    def performance_skill(self):
        result = 0
        if self.performance == True:
            result = self.cha_mod + self.proficiency
        elif self.performance_ex == True:
            result = self.cha_mod + ((self.proficiency)*2)
        else:
            result = self.cha_mod
        return result
    
    def persuasion_skill(self):
        result = 0
        if self.persuasion == True:
            result = self.cha_mod + self.proficiency
        elif self.persuasion_ex == True:
            result = self.cha_mod + ((self.proficiency)*2)
        else:
            result = self.cha_mod
        return result
    
    def religion_skill(self):
        result = 0
        if self.religion == True:
            result = self.int_mod + self.proficiency
        elif self.religion_ex == True:
            result = self.int_mod + ((self.proficiency)*2)
        else:
            result = self.int_mod
        return result
    
    def sleight_of_hand_skill(self):
        result = 0
        if self.sleight_of_hand == True:
            result = self.dex_mod + self.proficiency
        elif self.sleight_of_hand_ex == True:
            result = self.dex_mod + ((self.proficiency)*2)
        else:
            result = self.dex_mod
        return result
    
    def stealth_skill(self):
        result = 0
        if self.stealth == True:
            result = self.dex_mod + self.proficiency
        elif self.stealth_ex == True:
            result = self.dex_mod + ((self.proficiency)*2)
        else:
            result = self.dex_mod
        return result
    
    def survival_skill(self):
        result = 0
        if self.survival == True:
            result = self.wis_mod + self.proficiency
        elif self.survival_ex == True:
            result = self.wis_mod + ((self.proficiency)*2)
        else:
            result = self.wis_mod
        return result