from django.urls import path
from characters.views import (
    CharacterDetailView,
    CharacterListView,
    CharacterCreateView,
    CharacterUpdateView,
    CharacterDeleteView,
)

urlpatterns = [
    path("", CharacterListView.as_view(), name="character_list"),
    path("<int:pk>/", CharacterDetailView.as_view(), name="character_detail"),
    path("create/", CharacterCreateView.as_view(), name="character_create"),
    path("<int:pk>/update/", CharacterUpdateView.as_view(), name="character_update"),
    path("<int:pk>/delete/", CharacterDeleteView.as_view(), name="character_delete"),
]